import sys,os,newick3,phylo3

"""
Difference from pep:
- Use trimal instead of phyutility for triming alignments

start from fasttree results from the first round of alignment

not automating the first round so that one can check the data
adjust filters for all-by-all blast
and remove problematic clusters and process clusters that are unusually large

Make alignment and then tree
walk through the tree and cut branches longer than the first cutoff, re-align until
no branch is longer than the first cutoff

then cut branches longer than the second cutoff
re-align until no branch is longer than the second cutoff
"""

MIN_INGROUP_TAXA = 8
#all ingroup datasets have "@" after the four-character taxon name code
#will skip fasta file if the number of uniq taxon codes are less than this

NUM_MAFFT_CORES = 7
#number of processors mafft uses
#generally should be less than 10

#cutoff1 = 3.0
#cutoff2 = 1.0

cutoff1 = 2.0
cutoff2 = 0.5

#if taxon id pattern changes, change it here
def get_name(name):
	return name.replace("_R_","")[:4]
	
def get_leaf_labels(leaves):
	labels = []
	for i in leaves:
		labels.append(i.label)
	return labels

def count_ingroups(node):
	labels = get_leaf_labels(node.leaves())
	names = []
	for label in labels:
		name = get_name(label)
		if "@" in label and name not in names:
			names.append(name)
	return len(names)

#do not score terminal branches
#long terminal branches usually do not disturb the alignment as much
def find_longest_internal_branch_length(tree):
	longest_internal_branch_length = 0.0
	for node in tree.iternodes():
		if node != tree and not node.istip:
			longest_internal_branch_length = max(longest_internal_branch_length,node.length)
	return longest_internal_branch_length

#smooth the kink created by prunning
#to prevent creating orphaned tips after prunning twice at the same node
def remove_kink(node,curroot):
	if node == curroot and curroot.nchildren == 2:
		#move the root away to an adjacent none-tip internal node
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: #tree has >=4 leaves so the other node cannot be tip
			curroot = phylo3.reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot

#cut long branches and output all subtrees regardless of size
def cut_long_branches(curroot,cutoff):
	going = True
	subtrees = [] #store all subtrees after cutting
	if curroot.nchildren == 2: #fix the root
		#move the root away to an adjacent none-tip internal node
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: #tree has >=4 leaves so the other node cannot be tip
			curroot = phylo3.reroot(curroot,curroot.children[0])
	while going:
		going = False #only keep going if long branches were found during last round
		for node in curroot.iternodes(): #Walk through nodes
			if node != curroot and node.length > cutoff:
				subtrees.append(node)
				node = node.prune()
				if len(curroot.leaves()) >= 4:
					node,curroot = remove_kink(node,curroot)
					going = True
				break
	subtrees.append(curroot) #write out the residue after cutting
	return subtrees

def mafft_align(fasta_file):
	count = 0 #record how many sequences in the fasta file
	with open(fasta_file,"r") as infile:
		for line in infile:
			if line[0] == ">": count += 1
	if count >= 1000: alg = "--auto" #so that the run actually finishes!
	else: alg = "--genafpair --maxiterate 1000"
	com = "mafft "+alg+" --nuc --thread "+str(NUM_MAFFT_CORES)
	#com += " --treein "+treein
	com += " "+fasta_file+" > "+fasta_file+".aln"
	print com
	os.system(com)
	return fasta_file+".aln"

def count_info_sites(seq):
	seq = seq.replace("-","")
	seq_removen = seq.replace("n","")
	return len(seq_removen.replace("N",""))

#DNA matrix tend to have many very small fragments
def trim_alignment(alignment,occupancy):
	#cmd = "phyutility -clean 0.1 -in "+alignment+" -out "+alignment+"-pht"
	cmd = "trimal -in "+alignment+" -out "+alignment+"-trim -gt "+str(occupancy)+" -st 0.001"
	print cmd
	os.system(cmd)	
	seqid = ""
	infile = open(alignment+"-trim","r")
	outfile = open(alignment+"-cln","w")
	first = True
	for line in infile:
		line = line.strip()
		if len(line) == 0: continue #skip empty lines
		if line[0] == ">":
			if seqid != "": #not at the first line
				if first == True:
					first = False
					min_chr = max(occupancy*len(seq),100)
					print "cut matrix",min_chr,len(seq)
				num_informative_sites = count_info_sites(seq)
				if num_informative_sites >= min_chr:
					outfile.write(">"+seqid+"\n"+seq+"\n")
			seqid,seq = line.split(" ")[0][1:],""
		else: seq += line.strip()
	#process the last seq
	num_informative_sites = count_info_sites(seq)
	if num_informative_sites >= min_chr:
		outfile.write(">"+seqid+"\n"+seq+"\n")
	infile.close()
	outfile.close()
	return alignment+"-cln"

def fasttree(cleaned_alignment):
	com = "FastTreeMP -nt -gtr -gamma "+cleaned_alignment+" >"+cleaned_alignment+".tre"
	print com
	os.system(com)
	return cleaned_alignment+".tre"

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python pep_cut_long_branches_iter.py DIR"
		sys.exit(0)
	
	DIR = sys.argv[1]+"/"
	for i in os.listdir(DIR): #go through fasta files in the input directory
		if i[-9:] != ".fasttree": continue
		fasta_file = DIR+i.replace(".aln-cln.fasttree","")
		tree_file = DIR+i
		with open(tree_file,"r") as infile:
			first_line = infile.readline() #there's only 1 tree in each file
		if first_line.strip() == "": continue #empty file after trimming
		intree = newick3.parse(first_line.replace ("-","_"))
		if count_ingroups(intree) < MIN_INGROUP_TAXA:
			continue #skip trees with few ingroups
		ccID = (i.split(".")[0]).replace("_R","") #looks like cc9

		#if intree has no long branches at all jus use the original fasta and alignment
		longest_branch_length = 0.0
		for node in intree.iternodes():
			if node != intree:
				longest_branch_length = max(longest_branch_length,node.length)
		if longest_branch_length < cutoff2:
			cmd = "cp "+fasta_file+" "+fasta_file.replace("_R.fa","-1.final.fa")
			os.system(cmd)
			print cmd
			print fasta_file
			aln_file = fasta_file+".aln"
			cmd = "cp "+aln_file+" "+aln_file.replace("_R.fa.aln","-1.final.fa.aln")
			os.system(cmd)
			print cmd
			continue
		
		trees = [intree]

		#read fasta file
		infile = open(fasta_file,"r")
		seqDICT = {} #key is seq id, value is the sequence
		seqid = ""
		for line in infile:
			line = line.strip()
			if len(line) > 0:
				if line[0] == ">":
					if seqid != "":
						seqDICT[seqid] = seq
						if "-" in seqid: print seqid
					seqid,seq = line[1:].replace("-","_"),""
				else: seq += line.strip()
		seqDICT[seqid] = seq #add the last record
		infile.close()

		#cut by the first cutoff
		print i,"Cutting branches longer than",cutoff1
		outfile = open(DIR+ccID+".cut1.trees","w")
		newtrees = [] #store trees in need of cutting for the next round
		count = 0
		while True:
			for tree in trees:
				if find_longest_internal_branch_length(tree) < cutoff1:
					outfile.write(newick3.tostring(tree) +";\n")
					#can be the original tree or cut tree. no need to cut tip here
				else:
					subtrees = cut_long_branches(tree,cutoff1)
					for subtree in subtrees:
						if count_ingroups(subtree) < MIN_INGROUP_TAXA: continue
						count += 1
						newname = DIR+ccID+".cut1-"+str(count)
						with open(newname+".cutbranch","w") as outfile1: #record the cut branch
							outfile1.write(newick3.tostring(subtree)+";\n")
						with open(newname+".fa","w") as outfile2: #output fasta
							for label in get_leaf_labels(subtree.leaves()):
								outfile2.write(">"+label+"\n"+seqDICT[label]+"\n")
						newaln = mafft_align(newname+".fa")
						newcln = trim_alignment(newaln,0.1) #low occupancy
						newtreefile = fasttree(newcln)
						with open(newtreefile) as infile: #update the tree
							newtrees.append(newick3.parse(infile.readline()))
			if newtrees == []: break
			trees = newtrees
			newtrees = []
		outfile.close()

		trees = [] #update the tree record for the next round
		with open(DIR+ccID+".cut1.trees","r") as infile:
			for line in infile:
				if len(line) > 3:
					trees.append(newick3.parse(line))
		
		#cut by the second cutoff
		print len(trees),"trees cutting by",cutoff2
		outfile = open(DIR+ccID+".cut2-final.trees","w")
		count = 0
		newtrees = [] #store trees in need of cutting for the next round
		while True:
			for tree in trees:
				print find_longest_internal_branch_length(tree)
				if find_longest_internal_branch_length(tree) < cutoff2:
					subtrees = cut_long_branches(tree,cutoff2)
					for subtree in subtrees:
						if count_ingroups(subtree) >= MIN_INGROUP_TAXA:
							outfile.write(newick3.tostring(subtree)+";\n")
				else:
					subtrees = cut_long_branches(tree,cutoff2)
					print len(subtrees)
					for subtree in subtrees:
						if count_ingroups(subtree) < MIN_INGROUP_TAXA: continue
						count += 1
						newname = DIR+ccID+".cut2-"+str(count)
						with open(newname+".cutbranch","w") as outfile1:
							outfile1.write(newick3.tostring(subtree)+";\n")
						with open(newname+".fa","w") as outfile2: #output fasta
							for label in get_leaf_labels(subtree.leaves()):
								outfile2.write(">"+label+"\n"+seqDICT[label]+"\n")
						newaln = mafft_align(newname+".fa")
						newcln = trim_alignment(newaln,0.3)#higher occupancy
						newtreefile = fasttree(newcln)
						with open(newtreefile) as infile: #update the tree
							newtrees.append(newick3.parse(infile.readline()))
			if newtrees == []: break
			trees = newtrees
			newtrees = []
		outfile.close()

		with open(DIR+ccID+".cut2-final.trees","r") as infile:
			count = 1
			for line in infile:
				if len(line) <= 3: continue
				tree = newick3.parse(line)
				newfasta = DIR+ccID+"-"+str(count)+".final.fa"
				with open(newfasta,"w") as outfile2: #output fasta
					for label in get_leaf_labels(tree.leaves()):
						outfile2.write(">"+label+"\n"+seqDICT[label]+"\n")
				count += 1

