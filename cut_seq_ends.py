"""
input:
- a fasta file with all sequences used for all-by-all blast
- a file with all the unfiltered results from all-by-all blastn or blastp
Currently assume that query and hit are the same direction

Ignore hits from the same taxa
Check for ends that doesn't have any hits in any other taxa

output:
- .cutinfo file with start and end of seq to keep, and size of seq
- .cut file with all the sequences after cutting
"""

import os,sys
from Bio import SeqIO

MIN_SEQ_LEN = 40

#if taxon id pattern changes, change it here
def get_taxon_name(name):
	return name[:4]

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: cut_seq_ends.py fasta blast_output"
		sys.exit()
	
	print "Reading raw blast output"
	
	cutDICT = {} #key is seqid, value is a list [start,end,length]
	with open(sys.argv[2],"r") as infile:
		for line in infile:
			if len(line) < 3:
				continue #skip empty lines
			spls = line.strip().split("\t")
			query,hit = spls[0],spls[2]
			if get_taxon_name(query) == get_taxon_name(hit):
				continue #ignore hits from the same taxa
			qlen,qstart,qend = int(spls[1]),int(spls[10]),int(spls[11])
			slen,sstart,send = int(spls[3]),int(spls[12]),int(spls[13])

			#get the widest range
			if query not in cutDICT:
				cutDICT[query] = [10000000,1,qlen] #[start,end,qlen]
			if hit not in cutDICT:
				cutDICT[hit] = [10000000,1,slen] #[start,end,slen]
			cutDICT[query][0] = min(cutDICT[query][0],qstart,qend) #compare starts
			cutDICT[query][1] = max(cutDICT[query][1],qstart,qend) #compare ends
			cutDICT[hit][0] = min(cutDICT[hit][0],sstart,send) #compare starts
			cutDICT[hit][1] = max(cutDICT[hit][1],sstart,send) #compare ends

	#output seqid, start and end for cutting, and seq length
	with open(sys.argv[2]+".cutinfo","w") as outfile:
		for seqid in cutDICT:
			cut = cutDICT[seqid] #[start,end,length]
			start,end,length = cut[0],cut[1],cut[2]
			if start != 1 or end != length:
				outfile.write(seqid+"\t"+str(start)+"\t"+str(end)+"\t"+str(length)+"\n")
	print "Output written to",sys.argv[1]+".cutinfo"

	print "Cutting"
	outfile = open(sys.argv[1]+".cut","w")
	with open(sys.argv[1],"rU") as handle:
		for record in SeqIO.parse(handle,"fasta"):
			seqid,seq = str(record.id),str(record.seq)
			if seqid in cutDICT:
				cut = cutDICT[seqid]
				start = cut[0]-1
				end = cut[1]
				seq = seq[start:end]
			if len(seq) >= MIN_SEQ_LEN:
				outfile.write(">"+seqid+"\n"+seq+"\n")
	outfile.close()
