"""
map ratio of #dup/#observe from extracted clades
to the rooted species tree

Change the value of BOOT_FILTER accordingly
"""

import phylo3,newick3,os,sys,math

BOOT_FILTER = 80.0 #only look at clades with at least this average bootstrap
CARY = ["BERS","BJKT","EDIT","GJNX","HZTS","OMYK","AAXJ","BWRK","CBJR","CUTE",
		 "EYRD","FVXD","HDSY","OHKC","ONLQ","PDQH","SMMC","WGET","WMLW","XSSD",
		 "ZBPY","CTYH","CPKP","JLOV","FZQN","OLES","RXEN","SHEZ","SKNL","TJES",
		 "WPYJ","YNFJ","KJAA","NXTS","RNBN","SCAO","WQUF","EGOS","HMFE","JAFJ",
		 "JGAB","VJPU","ZBTA","AZBL","RUUB","BKQU","MRKX","SFKQ","CGGO","WOBD",
		 "FYSJ","BLWH","CPLT","EZGR","GCYL","IWIS","KDCH","LLQV","UQCB","GIWN",
		 "CVDF","LKKX","ILU1","ILU2","ILU3","ILU4","ILU5","ILU6","BVGI"]
ROS = ["Alyr","Atha","Brap","Ccle","Cpap","Crub","Csat","Csin","Egra","Fves","Gmax",
	   "Grai","Lusi","Mdom","Mesc","Mtru","Pper","Ptri","Pvul","Rcom","Tcac","Thal"]
AST = ["Slyc","Stub","Mgut"]

#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.replace("_R_","")[:4]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

def pass_boot_filter(node):
	total = 0.0
	count = 0
	for i in node.iternodes():
		if not i.istip and i.parent != None:
			total += float(i.label)
			count += 1
	if count == 0: #extracted clades with only two tips
		return True
	boot_average = total / float(count)
	print boot_average
	return boot_average >= BOOT_FILTER
	
def map_dups(DIR,tree_file_ending,sptree,nodeDICT):
	count = 0 #record number of trees that pass BOOT_FILTER
	for treefile in os.listdir(DIR):
		nodes = [] #store unique nodes that need to be counted
		#this way no node in nested dup will be counted multiple times
		if treefile[-len(tree_file_ending):] == tree_file_ending:
			print treefile
			with open(DIR+treefile,"r") as infile:
				gentree = newick3.parse(infile.readline()) #one tree only
			if pass_boot_filter(gentree):
				count += 1
				for gennode in gentree.iternodes():
					if gennode.nchildren != 2: continue #skip tip, but check the root
					child0 = gennode.children[0]
					child1 = gennode.children[1]
					if not child0.istip and not child1.istip:
						names0 = set(get_front_names(child0))
						names1 = set(get_front_names(child1))
						if len(set(names0).intersection(names1)) >= 2:
							genfront = set(get_front_names(gennode))
							mrca = phylo3.getMRCA(genfront,sptree)
							mrca.label = True #dup found
				#summarize all the dups found in this extracted clade
				#when nested dups found, only count once per extracted clade
				for spnode in sptree.iternodes():
					if not spnode.istip and spnode.label:
						if spnode not in nodeDICT: nodeDICT[spnode] = 0
						nodeDICT[spnode] += 1
						spnode.label = False #reset for next iteration
			else: print "failed bootstrap filter"
	return count,nodeDICT
					
if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python map_dups_bootfilter.py cary_cladeDIR ast_cladeDIR ros_cladeDIR rooted_spTree outputTree"
		sys.exit(0)
	
	caryDIR,astDIR,rosDIR = sys.argv[1]+"/",sys.argv[2]+"/",sys.argv[3]+"/"
	spTree = sys.argv[4]
	out = sys.argv[5]
	with open(spTree,"r") as infile:
		sptree = newick3.parse(infile.readline())
	nodeDICT = {} #key is internal nodes of the species tree, value is dup
	for spnode in sptree.iternodes():
		if not spnode.istip: spnode.label = False #no dup
	
	cary_count,nodeDICT = map_dups(caryDIR,".cary",sptree,nodeDICT)
	ros_count,nodeDICT  = map_dups(rosDIR,".ros",sptree,nodeDICT)
	ast_count,nodeDICT  = map_dups(astDIR,".ast",sptree,nodeDICT)
	
	#output a species tree with actual counts of duplications
	c = "/"+str(cary_count)
	r = "/"+str(ros_count)
	a= "/"+str(ast_count)
	for spnode in sptree.iternodes():
		if not spnode.istip:
			if spnode in nodeDICT:
				rep = get_front_names(spnode)[0]
				res = str(nodeDICT[spnode])
				if rep in CARY: spnode.label = res+c
				elif rep in ROS: spnode.label = res+r
				elif rep in AST: spnode.label = res+a
			else: spnode.label = None
	with open(out+".dup_count","w") as outfile:
		outfile.write(newick3.tostring(sptree)+";\n")
		
	#output another species tree with proportion of duplications
	c = float(cary_count)
	r = float(ros_count)
	a = float(ast_count)
	for spnode in sptree.iternodes():
		if not spnode.istip:
			if spnode in nodeDICT:
				rep = get_front_names(spnode)[0]
				res = nodeDICT[spnode]
				if rep in CARY: spnode.label = res/c
				elif rep in ROS:spnode.label = res/r
				elif rep in AST:spnode.label = res/a
			else: spnode.label = None
	with open(out+".dup_perc","w") as outfile:
		outfile.write(newick3.tostring(sptree)+";\n")


