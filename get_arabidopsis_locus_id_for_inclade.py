"""
Find one representative arabidopsis locus id for each inclade

Extract clade that didn't contain Aquilegia
Cut the basal duplciation if present
Output a file that on each line
inclade_name Arabidopsis_locus_ID
"""

import newick3,phylo3,os,sys
import collections

INCLADE_FILE_ENDING = ".cary"
HOMOTREE_ENDING = ".mm"
OUTGROUPS = ["Acoe"]
CARY = ["BERS","BJKT","EDIT","GJNX","HZTS","OMYK","AAXJ","BWRK","CBJR","CUTE",
		"EYRD","FVXD","HDSY","OHKC","ONLQ","PDQH","SMMC","WGET","WMLW","XSSD",
		"ZBPY","CTYH","CPKP","JLOV","FZQN","OLES","RXEN","SHEZ","SKNL","TJES",
		"WPYJ","YNFJ","KJAA","NXTS","RNBN","SCAO","WQUF","EGOS","HMFE","JAFJ",
		"JGAB","VJPU","ZBTA","AZBL","RUUB","BKQU","MRKX","SFKQ","CGGO","WOBD",
		"FYSJ","BLWH","CPLT","EZGR","GCYL","IWIS","KDCH","LLQV","UQCB","GIWN",
		"CVDF","LKKX","ILU1","ILU2","ILU3","ILU4","ILU5","ILU6","BVGI"]
MIN_INGROUP_TAXA = 5

#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.replace("_R_","")[:4]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_back_labels(node,root):
	all_labels = get_front_labels(root)
	front_labels = get_front_labels(node)
	return set(all_labels) - set(front_labels) #labels do not repeat
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

def get_back_names(node,root): #may include duplicates
	back_labels = get_back_labels(node,root)
	return [get_name(i) for i in back_labels]
	
def count_cary_names(node): #only count names with @
	labels = get_front_labels(node)
	cary_names = []
	for label in labels:
		if "@" in label:
			cary_names.append(get_name(label))
	return len(set(cary_names))
	
def remove_kink(node,curroot):
	if node == curroot and curroot.nchildren == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: curroot = phylo3.reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot
	
#input is a tree with both ingroups and more than 1 outgroups
def extract_ingroup_clades(root):
	print "extracting clades"
	inclades = []
	while True:
		max_score,direction,max_node = 0,"",None
		for node in root.iternodes():
			if node == root: continue
			front,back = 0,0
			front_names_set = set(get_front_names(node))
			for name in front_names_set:
				if name in OUTGROUPS:
					front = -1
					break
				else: front += 1
			back_names_set = set(get_back_names(node,root))
			for name in back_names_set:
				if name in OUTGROUPS:
					back = -1
					break
				else: back += 1
			if front > max_score:
				max_score,direction,max_node = front,"front",node
			if back > max_score:
				max_score,direction,max_node = back,"back",node
		#print max_score,direction
		if max_score >= MIN_INGROUP_TAXA:
			if direction == "front":
				inclades.append(max_node)
				kink = max_node.prune()
				if len(root.leaves()) > 3:
					newnode,root = remove_kink(kink,root)
				else: break
			elif direction == "back":
				par = max_node.parent
				par.remove_child(max_node)
				max_node.prune()
				inclades.append(phylo3.reroot(root,par))#flip dirction
				if len(max_node.leaves()) > 3:
					max_node,root = remove_kink(max_node,max_node)
				else: break
		else: break
	return inclades

def find_Arabidopsis_ID(clade,outfile):
	#clade vs Arabidopsus locus IDs can be many-to-many, or zero to zero
	#and any combinations in between
	#we want one AthaID only
	labels = get_front_labels(clade)
	incladeIDs,AthaID = [],""
	for label in labels:
		if label in incladeDICT: incladeIDs.append(incladeDICT[label])
		if get_name(label) == "Atha": AthaID = label
	print incladeIDs,AthaID
	if len(incladeIDs) > 0:
		for i in incladeIDs:
			outfile.write(i+"\t")
			if AthaID == "": outfile.write("Null\n")
			else: outfile.write((AthaID.split("__")[1]).split(".")[0]+"\n")
					
if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python get_arabidopsis_locus_id_for_inclade.py incladeDIR homologDIR outfile"
		sys.exit(0)

	incladeDIR = sys.argv[1]+"/" #files look like cc1-5.mm.1.cary or cc63-3.subtrees_2.mm.3.cary
	homologDIR = sys.argv[2]+"/" #files look like cc1-5.mm or cc63-3.subtrees_2.mm
	out = sys.argv[3]
	
	#initiate a dictionary to store all the mmIDs and incladeIDs
	homologDICT = {} #key is homolog name, value is a list of incladeIDs
	for incladeID in os.listdir(incladeDIR):
		if incladeID[-len(INCLADE_FILE_ENDING):] == INCLADE_FILE_ENDING:
			homologID = incladeID.split("mm")[0]+"mm"
			if homologID not in homologDICT:
				homologDICT[homologID] = []
			homologDICT[homologID].append(incladeID)
	
	outfile = open(out,"w")
	for homologID in homologDICT: #loop through all the homologIDs
		print "\n"+homologID
		with open(homologDIR+homologID,"r") as infile:
			 intree = newick3.parse(infile.readline())
		curroot = intree
		all_names = get_front_names(curroot)
		if "Atha" not in all_names: continue #ignore if no Arabidopsis

		incladeDICT = {} #key is the first seqID, value is the incladeID
		for incladeID in homologDICT[homologID]:
			with open(incladeDIR+incladeID,"r") as infile:
				inclade = newick3.parse(infile.readline())
			incladeDICT[get_front_labels(inclade)[0]] = incladeID

		#check whehter there's any outgroup at all
		outgroup_present = False
		for name in all_names:
			if name in OUTGROUPS:
				outgroup_present = True
				clades = extract_ingroup_clades(curroot)
				with open(out+homologID+".temp.tre","w") as outfile1:
					for i in clades:
						outfile1.write(newick3.tostring(i)+";\n")
				print len(clades),"clades extracted"
				for clade in clades: find_Arabidopsis_ID(clade,outfile)
				break
		if not outgroup_present: #homolog is small enough that just take the Atha it has
			find_Arabidopsis_ID(curroot,outfile)
	outfile.close()
