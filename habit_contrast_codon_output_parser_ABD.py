"""
prepare a taxon name table in which each line look like:
GJNX	Aizoaceae_Cypselea_humifusum_H

input trees are the cary clades with name fixed
input alignment are the output of pal2nal

label and unrooot the input rooted cary clades
run codeml
summarize codemel output
"""
import newick3,phylo3,os,sys
import collections

def get_name(label):
	return label[:4]
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

#value of (herbaceous - woody ) / min(woody,herbaceous)
def get_contrast(W,H):
	if W <= 0.0 or H <= 0.0:
		return "NULL"
	return (H-W)/min(W,H)
	
#get the average clade length (including the branch leading to the mrca)
def get_average_length(tipids,tree):
	mrca = None
	for nd in tree.iternodes(): #root to tip
		if set(get_front_labels(nd)) == set(tipids):
			mrca = nd
			break
	if mrca == None: #contrast is at the root. No way to calculate the value
		return None
	for node in mrca.iternodes(order=1):#POSTORDER,tip to root
		if node.istip: #tips
			node.data['len'] = node.length
		else: #internal nodes
			child0,child1 = node.children[0],node.children[1]
			above0,above1 = child0.data['len'],child1.data['len']
			node.data['len'] = ((above0+above1)/2.)+node.length
		if node == mrca:
			return node.data['len']

#return contrastdN,contrastdS,contrastdN/dS,WdN,WdS,Womega,HdN,HdS,HdN/dS,and taxa
def parse_codeml_output_tips(codeml_outfile,Wseqid,Hseqid):
	lines = [line.strip() for line in open(codeml_outfile)]
	if "Time used" not in lines[-1] and "Time used" not in lines[-2]:
		print "Unfinished run"
		return None #runs didn't finish
	for i in range(len(lines)):
		line = lines[i]
		if "w (dN/dS) for branches" in line:
			spls = line.split(' ')
			Womega,Homega = float(spls[-2]),float(spls[-1])
		elif "dS tree:" in line:
			tree = newick3.parse(lines[i+1])
			WdS = get_average_length(Wseqids,tree)
			HdS = get_average_length(Hseqids,tree)
		elif "dN tree:" in line:
			tree = newick3.parse(lines[i+1])
			WdN = get_average_length(Wseqids,tree)
			HdN = get_average_length(Hseqids,tree)
	if WdN != None and HdN != None and WdS != None and HdS != None:
		return get_contrast(WdN,HdN),get_contrast(WdS,HdS),WdN,WdS,Womega,HdN,HdS,Homega
	else: return None
	
if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python habit_contrast_codon.py codemlDIR outfile"
		sys.exit()
	
	DIR = sys.argv[1]+"/"
	outname = sys.argv[2]
	outfile = open(outname,"w")
	outfile.write("cladeID\tcontrastdN\tcontrastdS\tWdN\tWdS\tWomega\tHdN\tHdS\tHomega\ttaxa(woody;herbaceous)\n")
	contrastdNout,contrastdSout,Womegaout,Homegaout = [],[],[],[] #write out summary for plotting in R
	count = 0
	for i in os.listdir(DIR):
		if i[-4:] != ".out": continue
		cladeID = i.replace(".codeml.out","")
		print cladeID
		logfile = DIR+i.split("cary")[0]+"cary.log"
		shortIDspls = (cladeID.split(".cary.")[1]).split(".")
		shortID = shortIDspls[0]+"."+shortIDspls[1]
		with open(logfile,"r")as infile:
			Wseqids,Hseqids = [],[]
			for line in infile:
				if line.find(shortID) > -1:
					spls = line.strip().split(":")[1]
					spls = spls.split(";")
					Wseqids = (spls[0].strip(",")).split(",")
					Hseqids = (spls[1].strip(",")).split(",")
		if len(Wseqids) == 0 or len(Hseqids) == 0:
			print "Unfinished run"
			continue
		out = parse_codeml_output_tips(DIR+i,Wseqids,Hseqids)
		if out != None and count < 3000:
			count += 1
			contrastdN,contrastdS,WdN,WdS,Womega,HdN,HdS,Homega = parse_codeml_output_tips(DIR+i,Wseqids,Hseqids)
			if contrastdN > -5 and contrastdN < 10.: contrastdNout.append(contrastdN)
			if contrastdS > -5 and contrastdS < 10.: contrastdSout.append(contrastdS)
			if Womega >= 0 and Womega < 1.: Womegaout.append(Womega)
			if Homega >= 0 and Homega < 1.: Homegaout.append(Homega)
			outfile.write(cladeID+"\t"+str(contrastdN)+"\t"+str(contrastdS)+"\t")
			outfile.write(str(WdN)+"\t"+str(WdS)+"\t"+str(Womega)+"\t")
			outfile.write(str(HdN)+"\t"+str(HdS)+"\t"+str(Homega)+"\t")
			for Wseqid in Wseqids:
				outfile.write(Wseqid+",")
			outfile.write(";")
			for Hseqid in Hseqids:
				outfile.write(Hseqid+",")
			outfile.write("\n")
	outfile.close()
	
	#write out filtered and sorted output for plotting
	with open(outname+"_contrast_dN","w") as outfile:
		for i in sorted(contrastdNout):
			outfile.write(str(i)+"\n")
	with open(outname+"_contrast_dS","w") as outfile:
		for i in sorted(contrastdSout):
			outfile.write(str(i)+"\n")
	with open(outname+"_Womega","w") as outfile:
		for i in sorted(Womegaout):
			outfile.write(str(i)+"\n")
	with open(outname+"_Homega","w") as outfile:
		for i in sorted(Homegaout):
			outfile.write(str(i)+"\n")
			
	infile = open(outname,"r")
	outfile = open(outname+".equal_size","w")
	for line in infile:
		spls = line.split("\t")
		if len(spls) > 3 and line[:2] == "cc":
			count = spls[0].split(".")[-1]
			Wcount = (count.split("H")[0]).replace("W","")
			Hcount = count.split("H")[-1]
			if Wcount == Hcount: #are still strings but fine
				outfile.write(line)
	infile.close()
	outfile.close()
